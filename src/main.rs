use std::fmt;
use eframe::egui;

fn main() -> Result<(), eframe::Error>{
    let options = eframe::NativeOptions{
        viewport: egui::ViewportBuilder::default().with_inner_size([800_f32, 500_f32]),
        ..Default::default()
    };
    eframe::run_native(
        "Binary, Octal, and Hexidecimal convertor",
        options,
        Box::new(|_cc| {
            Box::<App>::default()
        })
    ) // returns
}

struct IOOption {
    inputs: Vec<String>,
    calc_step_outputs: Vec<String>,
    result_output: String
}

struct App{
    binary_io: IOOption,
    octal_io: IOOption,
    hexadecimal_io: IOOption,
}

#[derive(Debug)]
enum CalcOption {
    Binary,
    Octal,
    Hexadecimal,
}

impl fmt::Display for CalcOption {
    fn fmt(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        write!(formatter, "{:?}", self)
    }
}

impl Default for App {
    fn default() -> Self {
        Self {
            binary_io: IOOption{
                inputs: vec!["".to_string(); 8],
                calc_step_outputs: vec!["0".to_string(); 8],
                result_output: "".to_owned()
            },
            octal_io: IOOption{
                inputs: vec!["".to_string(); 4],
                calc_step_outputs: vec!["0".to_string(); 4],
                result_output: "".to_owned()
            },
            hexadecimal_io: IOOption{
                inputs: vec!["".to_string(); 2],
                calc_step_outputs: vec!["0".to_string(); 2],
                result_output: "".to_owned()
            },
        }
    }
}

impl App {
    fn add_calculation_grid(&mut self, ui: &mut egui::Ui, calc_option: CalcOption, base: i32, input_len: u32) {
        let inputs: &mut Vec<String> = match &calc_option {
            CalcOption::Binary => &mut self.binary_io.inputs,
            CalcOption::Octal => &mut self.octal_io.inputs,
            CalcOption::Hexadecimal => &mut self.hexadecimal_io.inputs,
        };
        ui.heading(format!("{} - Base {}", calc_option.to_string(), base.to_string()));
        egui::Grid::new(format!("grid_{}", calc_option.to_string())).show(ui, |ui| {
            ui.label("Multiplier");
            for i in (0..input_len).rev() {
                ui.label(format!("{}", base.pow(i)));
                if i != 0 {
                    ui.label(" ");
                }
            }
            ui.end_row();
            ui.label(format!("{}:", calc_option.to_string()));
            for i in (0..input_len).rev() {
                ui.text_edit_singleline(&mut inputs[i as usize]);
                if i != 0 {
                    ui.with_layout(egui::Layout::centered_and_justified(egui::Direction::TopDown), |ui| {
                        ui.label(" ");
                    });
                }
            }
            let calc_step: &mut Vec<String> = match &calc_option {
                CalcOption::Binary => &mut self.binary_io.calc_step_outputs,
                CalcOption::Octal => &mut self.octal_io.calc_step_outputs,
                CalcOption::Hexadecimal => &mut self.hexadecimal_io.calc_step_outputs,
            };
            ui.end_row();
            for i in (0..input_len).rev() {
                if i != input_len-1 {ui.label("+");} else {ui.label("");}
                ui.label(format!("{}",  calc_step[i as usize]));
            }
        });
        ui.with_layout(egui::Layout::left_to_right(egui::Align::TOP), |ui| {
            if ui.add(egui::Button::new("Calculate")).clicked() {
                self.calculate_result(&calc_option)
            }
            let output: &mut String = match &calc_option {
                CalcOption::Binary => &mut self.binary_io.result_output,
                CalcOption::Octal => &mut self.octal_io.result_output,
                CalcOption::Hexadecimal => &mut self.hexadecimal_io.result_output,
            };
            ui.label(format!("Result: {}", output));
        });
    }

    fn calculate_result(&mut self, option: &CalcOption) {
        let (base, char_set, inputs, output, calc_steps): (u32, Vec<&str>, &mut Vec<String>, &mut String, &mut Vec<String>) = match &option {
            CalcOption::Binary => (2, vec!["0", "1"], &mut self.binary_io.inputs, &mut self.binary_io.result_output, &mut self.binary_io.calc_step_outputs),
            CalcOption::Octal => (8, vec!["0", "1", "2", "3", "4", "5", "6", "7"], &mut self.octal_io.inputs, &mut self.octal_io.result_output, &mut self.octal_io.calc_step_outputs),
            CalcOption::Hexadecimal => (16, vec!["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F"], &mut self.hexadecimal_io.inputs, &mut self.hexadecimal_io.result_output, &mut self.hexadecimal_io.calc_step_outputs),
        };
        let mut valid_input: bool = true;
        for i in 0..inputs.len() {
            if ! char_set.contains(&inputs[i].as_str()) {
                valid_input = false;
                break;
            }
        }
        if valid_input {
            let mut output_str: String = "".to_owned();
            for i in (0..inputs.len()).rev() {
                output_str.push_str(&inputs[i]);
                calc_steps[i] = format!("{}", &inputs[i].parse::<u32>().unwrap() * base.pow(i as u32))
            }
            let output_number: i32 = i32::from_str_radix(&output_str, base).unwrap(); // error check not performed on unwrap, assumed valid from previous validation, this is probably a horrifically bad idea
            *output = format!("{}", output_number);
        } else {
            *output = "Value error has occurred, make sure to fill out all boxes, and that all inputs are valid".to_owned();
        }
    }

}

impl eframe::App for App{
    fn update(&mut self, ctx: &egui::Context, _frame: &mut eframe::Frame) {
        egui::CentralPanel::default().show(ctx, |ui| {
            // attempts must be made to reduce code repetition and modularise this.
            // modularisation task complete!
            self.add_calculation_grid(ui, CalcOption::Binary, 2, 8);
            ui.add_space(50_f32);
            self.add_calculation_grid(ui, CalcOption::Octal, 8, 4);
            ui.add_space(50_f32);
            self.add_calculation_grid(ui, CalcOption::Hexadecimal, 16, 2);
        });
    }
}